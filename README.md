Beamer Latex Example
====================

## Dependencies installation
`sudo apt install --{with/install}-recommends texlive-full`

## Compile PDF
`make talk.tex`

## Compile handouts
`make handouts`

## Sources
- [mertz.pdf](docs/mertz.pdf)
- [beameruserguide.pdf](docs/beameruserguide.pdf)
- [https://tex.stackexchange.com/q/105613](https://tex.stackexchange.com/q/105613)
- [https://tex.stackexchange.com/a/26478](https://tex.stackexchange.com/a/26478)
- [https://tex.stackexchange.com/a/244024](https://tex.stackexchange.com/a/244024)
