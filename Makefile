TARGET_FILE := talk
.DEFAULT_GOAL := $(TARGET_FILE).pdf

handouts.pdf: handouts.tex
	pdflatex $<
	pdflatex $<
	cat $@ > $(TARGET_FILE).pdf

.PHONY: handouts
handouts: $(TARGET_FILE).tex
	cat $(TARGET_FILE).tex | sed -e 's/\\documentclass\[/\\documentclass[handout,/g' > handouts.tex
	$(MAKE) handouts.pdf

$(TARGET_FILE).pdf: $(TARGET_FILE).tex
	pdflatex $(TARGET_FILE).tex
	pdflatex $(TARGET_FILE).tex

.PHONY: clean
clean:
	rm -f \
	*.log \
	*.snm \
	*.nav \
	*.toc \
	*.out \
	*.aux \
	$(TARGET_FILE).pdf \
	handouts.*


